#! /bin/bash

#########################################
# Script pour créer une image Docker compatible avec Ansible, et
# instanciant ensuite un conteneur Docker de test nommé conteneur_test
# Version 1.0 (08/01/2022)
#
# Fichier téléchargé depuis
# https://sagot.dev/ansible/conteneur-test.sh
#
# Auteur:
# Jules Sagot--Gentil (https://sagot.dev/)
#
# Lié à l'article: https://sagot.dev/articles/tester-facilement-ansible/
#
# License:
# CC BY-SA 4.0 (http://creativecommons.org/licenses/by-sa/4.0/)
#
#########################################

echo "Test de votre installation de Docker"
if docker run --rm hello-world:latest > /dev/null
   then
       echo "Docker est installé et fonctionne"
   else
       echo "Installez Docker et placez l'utilisateur courant dans le groupe Docker"
       exit 1
fi

echo
echo "Recherche de l'image de test et construction si nécéssaire"
if docker images | grep -qE "^debian[ ]+bullseye-python"
   then
       echo "L'image Docker de test a déjà été construite"
   else
       echo "Construction de l'image Docker de test"
       docker build --tag=debian:bullseye-python - <<EOF
FROM debian:bullseye
RUN apt update && apt install -y python3 python3-pip
STOPSIGNAL SIGKILL
CMD tail --follow /dev/null
EOF
       echo "Image de test construite avec succès"
fi

echo
echo "Lancement du conteneur de test"
destroy="N"
launch="O"
if docker ps -a | grep -q "conteneur_test"
then
    echo ""
    read -e -p "Le conteneur de test est déjà lancé, souhaitez vous le stopper, le détruire et en relancer un nouveau ? [O/N] " destroy
    [[ "$destroy" != [Oo]* ]] && exit 0
fi

if [[ "$destroy" == [Oo]* ]]
then
    echo ""
    echo "Le conteneur courant va être remplacé"
    docker stop conteneur_test && docker rm conteneur_test
fi

echo ""
echo "Lancement du conteneur de test"
docker run --detach --name=conteneur_test debian:bullseye-python
