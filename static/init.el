;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Configuration Emacs pour Typescript
; Version 1.0 (2022-06-06)
;
; Fichier téléchargé depuis
; https://sagot.dev/init.el
;
; Auteur:
; Jules Sagot--Gentil (https://sagot.dev/)
;
; License:
; CC BY-SA 4.0 (http://creativecommons.org/licenses/by-sa/4.0/)
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; Le paquet `package` est installé avec Emacs, on va l'utiliser pour
;; installer le paquet dap-mode
(require 'package)

;; Ajout de Melpa à la liste des dépôts Emacs
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)

;; Chargement des paquets installés
(package-initialize)

;; La variable package-archive-contents
;; contient la liste des paquets connus
(unless package-archive-contents (package-refresh-contents))

;; Définition de la liste des paquets à installer dans une nouvelle variable
(setq package-list '(dap-mode typescript-mode tree-sitter tree-sitter-langs lsp-mode lsp-ui))

;; Installation de chaque paquet défini dans package-list non installé 
(dolist (package package-list)
  (unless (package-installed-p package) (package-install package)))

;; Configuration de la coloration syntaxique:
;; Chargement des paquets
(require 'tree-sitter-langs)
(require 'tree-sitter)

;; La coloration syntaxique sera active dans tous les buffers Emacs
(global-tree-sitter-mode)
(add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode)

;; Configuration de LSP
(require 'lsp-mode)
(add-hook 'typescript-mode-hook 'lsp-deferred)
(add-hook 'javascript-mode-hook 'lsp-deferred)

;; Chargement du débogueur node lorsque le mode majeur est Typescript
(defun my-setup-dap-node ()
  "Require dap-node feature and run dap-node-setup if VSCode module isn't already installed"
  (require 'dap-node)
  (unless (file-exists-p dap-node-debug-path) (dap-node-setup)))
(add-hook 'javascript-mode-hook 'my-setup-dap-node)
(add-hook 'typescript-mode-hook 'my-setup-dap-node)
