---
title: "Détecter les disfonctionnements d'un piège avec Trap Manager"
date: 2021-11-05T18:00:00+01:00
tags: ["projets"]
type: "post"
image: "/trap_manager.webp"
expiryDate: 2024-02-19T17:00:00+0100
---

## Le projet Trap Manager

[Trap Manager](https://trap-manager.com/) est un projet mené par
[BRCSA](https://www.brcsa.com/fr/). BRCSA est une entreprise ayant
acquis une expertise dans la lutte contre les insectes. Leur coeur de
métier est la conception et la fabrication de pièges éliminant des
insectes nuisibles. Ces pièges à insectes sont placés dans des
bâtiments fermés dans le cadre d'une activité industrielle.

Trap Manager est un objet connecté qui équipe un piège à insectes. Ce
dispositif permet d'ajouter des fonctionnalités intelligentes au
piège:

- Comptage automatique du nombre d'insectes éliminés par le piège
- Photos des plaques de glues récoltant les insectes
- Catégorisation automatique des insectes tués (15 espèces peuvent être détectées)

[![Illustration du Trap Manager](/trap_manager.webp)](https://trap-manager.com/)

## Détection de panne du piège à insectes

Une nouvelle fonctionnalité à ajouter au Trap Manager est la détection de
disfonctionnements du piège. BRC a établi qu'il fallait détecter:

- Un piège déconnecté du réseau ou non alimenté
- La saturation en insectes de la plaque de glue
- Des tubes hors service

J'ai été chargé de la réalisation informatique de deux de ces alertes,
et nous allons étudier leurs implémentations dans cet article.

## Réalisation de la détection des pannes
J'ai choisi de réaliser la détection des pannes du côté serveur et non
sur l'ordinateur embarqué du [Trap Manager](https://trap-manager.com/)
car un Trap Manager déconnecté ou hors service ne peut pas avertir le
serveur de sa propre panne.

### Piège déconnecté ou hors service
Pour détecter si un piège est hors ligne, on vérifie s'il a récemment
remonté une image dans son dossier d'images ou non. Autrement dit, si
la date de modification de son dossier d'image est trop ancienne, on
considère le piège comme hors service.

J'ai ensuite réalisé un algorithme permettant d'alerter l'utilisateur
possédant un Trap Manager hors ligne par mail/sms une seule fois. Au
premier abord, on peut penser que l'utilisation d'une base de données
est indispensable pour cela. Mais j'ai préféré ajouter une notion
d'intervalle de temps dans lequel l'utilisateur peut être alerté, car
le script est lancé à un intervalle régulier.

![Vérification régulière pièges hors ligne](/trap_manager_offline_schedule.webp)

Dans l'exemple ci-dessus, le script de vérification des pièges hors
ligne est lancé à la 5ème minute de chaque heure. Mon système de
détection de piège hors ligne a été paramétré pour qu'un Trap Manager
soit considéré hors ligne au bout de 2h d'inactivité, avec
vérification de l'activité toutes les heures. 

Le Trap Manager a émis une dernière image à 1:05 et subit une avarie
qui le passe hors ligne. La vérification de 1:20 et celle de 2:20 ne
vont pas avertir l'utilisateur. C'est la vérification de 3:20 qui va
produire l'alerte car le piège a émis il y a plus de 2h (avant 1:20)
et il y a moins de 3h (après 0:20).

### Tubes hors service

![Feuille collante](/sticky_sheet.webp)

Cette image capturée par un [Trap Manager](https://trap-manager.com/)
met en évidence les deux tubes du pièges. Chacun de ces tubes apporte
de la luminosité à l'image capturée par le Trap Manager.

Pour détecter des tubes hors service, on va chercher à voir si la
dernière image capturée est noire ou très sombre. Pour cela, on va
calculer la luminosité de l'image.

Pour réaliser des opérations sur les images, dont le calcul de la
luminosité, j'utilise une librairie PHP: Imagick.

## Conclusion

J'ai réalisé cette mission en 5 jours ouvrés étalés sur deux semaines
(entre la signature du devis et la facturation). J'aime réaliser des
missions courtes comme celle-ci car elles permettent d'apporter
rapidement de la valeur à un projet que je connais bien.

A la date où j'écris cet article, je suis disponible pour une mission
de plus d'un mois de préférence sur la région Nantaise ou à Brest,
n'hésitez pas à me contacter par mail: jules at sagot point dev. Vous
pouvez également me retrouver sur
[Linkedin](https://www.linkedin.com/in/jules-sagot-gentil-2376b31b9/)
et [Malt](https://www.malt.fr/profile/julessg).
