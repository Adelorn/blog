---
title: "Devez vous utiliser les rôles Ansible dans votre projet ?"
slug: factoriser-playbook-ansible
date: 2022-09-03T07:00:00+0200
tags: ["ansible"]
type: "post"
image: "/ansible.jpeg"
description: >
  Vous avez écrit vos premiers playbooks Ansible pour votre projet web,
  et vous vous posez la question de la pertinence de l'utilisation de rôles ?
  Voici quelques raisons d'utiliser cette fonctionnalité
---

## Définition d'un rôle Ansible

Un [rôle
Ansible](https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html)
est un jeu de tâches réutilisables que vous pouvez **utiliser dans
plusieurs playbooks**, voir même **partager avec la communauté**
[Ansible
Galaxy](https://docs.ansible.com/ansible/latest/galaxy/user_guide.html)
si le rôle que vous avez écrit est assez générique pour répondre aux
besoins de développeurs écrivant un projet similaire. Vous pouvez
également **utiliser des rôles publiés** dans Ansible Galaxy, ce qui
vous fera **gagner du temps** et vous donnera accès à un **code
utilisé par des milliers de développeurs**

## Quels sont les signes que vous devriez utiliser les rôles Ansible dans votre projet ?

+ Votre playbook est **long** et comporte la logique de déploiement de
  plusieurs **services complexes**
+ Vous devez écrire un **nouveau playbook** ayant un **cas d'usage
  similaire** à un playbook existant
+ Une partie de la logique de votre playbook peut être **remplacée par
  un rôle de la communauté Ansible** (par exemple: déploiement
  d'un cluster Docker Swarm)

## Étude de cas

### Déploiement de deux applications web avec des playbooks

Vous avez deux applications web utilisant la même base de données avec
deux utilisateurs distincts à déployer. Voici à quoi pourrait
ressembler un playbook d'installation d'un site pour cette tâche:

1. Installer et configurer la base de données, ajouter un utilisateur
   spécifique à l'application web
2. Installer [Nginx](https://docs.nginx.com/), un site pour [supporter
   les challenge
   ACME](https://community.letsencrypt.org/t/how-to-nginx-configuration-to-enable-acme-challenge-support-on-all-http-virtual-hosts/5622),
   un site spécifique à l'application web
3. Installation, configuration et récupération d'un certificat SSL
   gratuit avec [LetsEncrypt](https://letsencrypt.org/) pour le
   domaine du site

**Sans les rôles**, cette logique devra être **répétée**
(copiée/collée) dans chacun de vos deux playbooks d'installation
d'application web. Vous devrez également **écrire et dupliquer la
tâche complexe** de mise en place de LetsEncrypt: lancement de la
commande `certbot`, renouvellement de certificats, etc

### Comment les rôles Ansible peuvent améliorer votre processus de déploiement

Il serait bienvenu d'ajouter deux rôles au projet:

+ Un rôle base de données responsable de son
  installation/configuration ainsi de l'ajout d'un utilisateur via une
  variable passée au rôle
+ Un rôle Nginx responsable de la configuration de base (par ex
  support du challenge ACME) et de l'installation de sites également
  via une variable passée au rôle (nom/paramètres de la template de site)

Pour la gestion des certificats SSL, on va utiliser un rôle écrit et
maintenu par la **communauté Ansible**. Cela nous permettra de
bénéficier d'un code **générique, écrit par des experts et testé par
des milliers de personnes**, ainsi que de gagner du temps. Le code
d'un rôle communautaire est toujours disponible en ligne, et est
généralement court et compréhensible, je vous invite à le **lire avant
de l'installer** sur votre projet pour avoir une bonne idée de son
fonctionnement: [exemple du code source du module
certbot](https://github.com/geerlingguy/ansible-role-certbot/)


Voici ce que devient la logique de chaque playbook une fois qu'on a
écrit les deux rôles ci-dessus et installé le rôle communautaire
Certbot:

1. Configuration des variables de chaque rôle, par exemple
   l'utilisateur de base de données, le nom de la template Nginx, ses
   paramètres, etc
2. Import du __rôle base de données__
3. Import du __rôle Nginx__
4. Import du __rôle communautaire__ pour obtenir un certificat via
   LetsEncrypt
5. Exécution de __logique spécifique__ au service web

Le processus de **déploiement est ainsi amélioré**: vous avez écrit
une fois le code permettant de configurer les deux services base de
données et proxy Nginx, et pour la configuration LetsEncrypt il a
suffi de lire le code et configurer les variables du rôle obtenu via
**Ansible Galaxy**
