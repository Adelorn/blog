---
title: Connectons nous sur Mastodon
date: 2022-01-29T16:00:00+01:00
tags: ["trucs-et-astuces"]
type: post
image: "https://sagot.dev/mastodon-jules-sagot.jpeg"
description: Rejoignez moi sur Mastodon, un réseau social décentralisé
---

## TLDR;

Mastodon est un logiciel libre et auto-hébergé qui permet de faire du
micro blogging. C'est un **concurrent direct de Twitter**. Je vous propose
de me [rejoindre sur Mastodon](https://pouet.chapril.org/@sagotgej),
et de [vous créer un compte](https://pouet.chapril.org/about) si vous
n'en avez par encore un.

![Mastodon Jules Sagot](/mastodon-jules-sagot.jpeg)

## Comment s'inscrire sur Mastodon

Pour rejoindre ce réseau social, il faut d'abord rejoindre une
**instance** (= un serveur) de Mastodon. Ce serveur peut être hébergé par
n'importe qui: un particulier, une association ou une entreprise
par exemple.

## Comment choisir une instance

Les membres inscrits sur l'instance de votre choix forment votre
**réseau primaire**. Vos posts publics seront visibles sur le flux
public de l'instance, et comme une instance compte un nombre
raisonnable de membres (300 actifs dans mon instance par ex), il est
plus facile de **former son réseau avec les membres locaux** de votre
instance.

Une instance peut regrouper ses membres autour d'un **thème partagé**
par la communauté (loisir, humour, politique, métier, etc).

Pour ma part, j'ai choisi de rejoindre l'instance de
l'[April](https://pouet.chapril.org/about), une association qui
promeut le logiciel libre. Les utilisateurs de cette instance postent
régulièrement des articles sur le développement informatique et les
logiciels libres car **ce thème les passionne**.

Si vous lisez ces lignes, j'imagine que le logiciel libre compte pour
vous, et donc que [l'instance Mastodon de
l'April](https://pouet.chapril.org/about) vous irait bien. Si vous
être curieux d'explorer les autres instances existantes,
[@TheKinrar](https://mastodon.xyz/@TheKinrar) a réalisé un site web
pour [explorer les instances Mastodon](https://instances.social/).

## La fédération unit les membres des différentes instances

Vous avez rejoint une instance Mastodon, qui compte un certain nombre
de membres. Mais un problème se pose alors: comment suivre
[@TheKinrar](https://mastodon.xyz/@TheKinrar) (qui a réalisé un
chouette site ci-dessus) alors qu'il a rejoint l'instance
[mastodon.xyz](https://mastodon.xyz/), et non la mienne ? C'est ici
qu'intervient la notion de **fédération**, rendue possible par le
protocole [ActivityPub](https://en.wikipedia.org/wiki/ActivityPub)
(illustration ci-dessous par [@mray](https://social.tchncs.de/@mray)).

![ActivityPub](/ActivityPub-tutorial-image.png)

Lors de mon abonnement à @TheKinrar, mon instance, `pouet.chapril.org`
va prévenir son instance, `mastodon.xyz` que je suis désormais son
compte. Ensuite, lorsque @TheKinrar poste un billet (appelé `toot` ou
`pouet`) sur son profil, son instance expédiera ce billet vers mon
instance, et le billet se retrouvera dans mon fil d'actualités.

La fédération permet ainsi de relier les instances Mastodon entre
elles, ce qui forme un réseau d'instances.

## Pourquoi me suivre sur Mastodon

Je poste régulièrement des **résumés d'articles tech** lorsque je réalise
ma **veille technologique**.

### Mes derniers résumés d'articles

{{< mastodon toot_id="107705643129203217">}}

{{< mastodon toot_id="107693191817117630">}}

## Recommandations de comptes à suivre

Voici une courte liste de mes comptes préférés:

- [@tuxicoman](https://social.jesuislibre.net/@tuxicoman) _Blog tech_
- [@journalduhacker](https://framapiaf.org/@journalduhacker) _News tech
  francophones_
- [@CuisineLibre](https://mamot.fr/@CuisineLibre) _Recettes de cuisines_
- [@underscore](https://m.g3l.org/@underscore) _Podcast tech_
- [@kvuilleumier](https://framapiaf.org/@kvuilleumier) _Blog tech_
- [@p4bl0](https://mamot.fr/@p4bl0) _Maître de conférences en
  informatique_
- [@feditips](https://mstdn.social/@feditips) _Astuces du fédiverse_

Pour agrandir votre réseau, il est facile de trouver des profils à
suivre via [l'annuaire des
profils](https://docs.joinmastodon.org/user/discoverability/) fourni
par Mastodon.

Et vous, quels sont vos comptes préférés à suivre sur Mastodon ? :point_down:
