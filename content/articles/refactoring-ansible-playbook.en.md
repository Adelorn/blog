---
title: "When should you refactor your Ansible Playbooks into roles ?"
date: 2022-09-03T07:00:00+0200
tags: ["ansible"]
type: "post"
image: "/ansible.jpeg"
description: >
  You have heard about Ansible roles, but don't know if you should use
  them for your existing playbooks ? Here are some reasons to start
  using roles in your project
---

## Short definition of a role

An [Ansible
role](https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html)
is a set of **reusable tasks** that you can use into multiple
playbooks and even **share with the community** using [Ansible
Galaxy](https://docs.ansible.com/ansible/latest/galaxy/user_guide.html)

## When should you refactor your playbooks into roles ?

+ You mix **different concerns** about your app in a single playbook
+ Your playbook is getting **too complex**
+ You have to write a new playbook for an **use case similar** to an
  existing playbook
+ Some logic of your playbook can be **replaced by community
  maintained** roles (Docker Swarm setup for instance)

## Real world example

### Deploy two web applications with playbooks

Imagine you have to deploy two web applications connected to the same
database. Here is the example deployment scheme you have:

1. Install and configure the database, add a database user for each web app
2. Install [Nginx](https://docs.nginx.com/) with [ACME challenge
   support](https://community.letsencrypt.org/t/how-to-nginx-configuration-to-enable-acme-challenge-support-on-all-http-virtual-hosts/5622)
3. Use let's encrypt to get a SSL certificate for each web app domains
4. Deploy both web applications

You would want to write two playbooks, one for each web application
deployment. If you don't use roles, **you would have to copy/paste** the
database setup logic to both of the playbooks. In order to setup
trusted and free SSL certificate, **you would have to write few blocks
of scripts** with shell calls to [LetsEncrypt](https://letsencrypt.org/) certbot, handle certificate
renewal, etc.

### How Ansible roles can improve this deployment process

We could write two roles:

+ A database role responsible of the database setup and database users
  management
+ A Nginx role which handle core configuration (such as ACME challenge
  support) and manage available sites templates

We will use also an [Ansible community
role](https://galaxy.ansible.com/geerlingguy/certbot) for configuring
Let's encrypt and obtaining the certificates. This will **save us a
lot of time and efforts** implementing LetsEncrypt logic, and benefit
from the **expertise of the community** that wrote the
playbook. Usually the source code of an Ansible role is short and well
written, so don't hesitate to take a look at the [source code
available on
Github](https://github.com/geerlingguy/ansible-role-certbot/)

Here is the new playbooks we have for each web application:

1. Configure the roles variables such as database user, nginx site
   template name, etc
2. Import project __database role__
3. Import project __Nginx role__
4. Import __community role for let's encrypt__
5. Run __specific logic__ for this web application deployment

The deployment process have been **improved using roles**: you wrote once
how to install the database and Nginx proxy, as well as adding users
to the database and sites to the proxy. **Ansible Galaxy** brought great
quality code to your project so you just have to configure LetsEncrypt
role variables to get your free SSL certificates
