---
title: "Installer Ansible avec Python3 (Ubuntu, Debian)"
date: 2021-12-12T21:30:00+02:00
tags: ["ansible"]
type: "post"
description: >
  Installez Ansible au niveau de votre projet avec Python3 et son
  gestionnaire de paquets Pip.
---

Vous souhaitez démarrer ou utiliser un projet Ansible ? Cet article
présente comment installer [Ansible](https://ansible.com/) au niveau
de votre projet à l'aide de [Python 3](https://python.org/) et du
module `venv` sur Debian ou Ubuntu.

## Installation de Python et de son gestionnaire de dépendances

On besoin de Python et d'un gestionnaire gestionnaire de paquets
Python pour installer Ansible. Dans ce tutoriel, on utilise
[Pip](https://pypi.org/project/pip/) en tant que gestionnaire de
paquets. Sur Debian ou Ubuntu, vous pouvez l'installer avec apt:

{{< highlight sh>}}
sudo apt update
sudo apt install python3 python3-pip python3-venv
{{< /highlight>}}

## Création d'un virtualenv à la racine de votre projet

Ouvrez un terminal dans le dossier racine de votre projet:

{{< highlight sh>}}
# optionnel: mkdir -p ~/chemin/de/votre-projet
cd ~/chemin/de/votre-projet
{{< /highlight>}}

Puis créez un virtualenv que l'on nommera `.venv` qui contiendra les
paquets Python utiles à ce projet (dont Ansible). Pour cela, on fait
appel au module Python venv (option `-m venv`) précédemment installé
sur votre machine auquel on passe en argument le nom du dossier qui
contiendra le virtualenv Python (ici `.venv`).

{{< highlight sh>}}
python3 -m venv .venv
{{< /highlight>}}

Ensuite, il faut activer le virtualenv. Une fois le virtualenv activé,
votre shell n'utilisera plus l'interpréteur Python situé
`/usr/bin/python3`. À la place, le programme Python situé dans le
virtualenv sera utilisé. Une fois le virtualenv activé:

+ Toutes les installations de librairies Python réalisées avec Pip
  seront installées dans le virtualenv
+ Les librairies disponibles à l'import seront celles installées dans le virtualenv
+ Le programme Python situé `/usr/bin/python3` n'aura pas accès aux
  librairies installées dans le virtualenv

Utiliser un environnement virtuel Python vous permet d'isoler les
paquets entre vos différents projets. Cela permer d'éviter les
problèmes de conflits de dépendances Python entre projets.

{{< highlight sh>}}
which python3
# résultat: /usr/bin/python3

source .venv/bin/activate

which python3
# résultat: ~/chemin/de/votre-projet/.venv/bin/python3
{{< /highlight>}}

## Installation d'Ansible dans le virtualenv

Maintenant que nous avons créé notre virtualenv, nous allons installer
les packages Python Ansible et
[wheel](https://pypi.org/project/wheel/) (dépendance) en
utilisant Pip:

{{< highlight sh>}}
pip3 install wheel ansible
{{< /highlight>}}

Pour vérifier qu'Ansible est bien installé, vous pouvez afficher la
version du logiciel ou tester la connexion à vous même avec le [module
ansible
ping](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/ping_module.html):

{{< highlight sh>}}
ansible --version
# résultat:
# ansible [core 2.12.1]
#   ...

ansible localhost -m ping
# résultat:
# localhost | SUCCESS => {
#     "changed": false,
#     "ping": "pong"
# }
{{< /highlight>}}

## Nota bene

Pour pouvoir utiliser Ansible, vous devez avoir activé l'environnement
Python de votre projet avec la commande `source
.venv/bin/activate`. Vous devrez faire cette action à chaque fois que
vous utilisez un nouveau terminal.

Vous pouvez quitter le virtualenv activé avec la commande `deactivate`.

## Le mot de la fin

Vous avez maintenant Ansible installé dans l'environnement virtuel de
votre projet courant.

En vous souhaitant que votre projet infrastructure as code soit aussi
propre que cette installation d'Ansible :smile:
