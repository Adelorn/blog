---
title: "Installer PostgreSQL pour le développement - Debian 11"
date: 2022-06-02T09:00:00+0200
tags: ["javascript"]
type: "post"
description: >
 Installez PostgreSQL sur Debian 11, créez une première base de
 données et un rôle pour tester votre application
image: "https://www.postgresql.org/media/img/about/press/elephant.png"
image_type: "summary"
---

## Installation de PostgreSQL

La version couramment supportée par Debian est PostgreSQL 13. Le
paquet à installer est `postgresql`. C'est un
[métapaquet](https://debian-handbook.info/browse/fr-FR/stable/sect.building-first-package.html#id-1.18.5.2)
qui contient le client/serveur PostgreSQL en dépendance

Je vous conseille également de télécharger le paquet de documentation
au format HTML contenu dans le paquet `postgresql-doc`:

`sudo apt install postgresql postgresql-doc`

### Création d'un lien vers la documentation PostgreSQL

Une fois le paquet de documentation installé, vous pouvez l'ouvrir
avec `firefox https://www.postgresql.org/docs/13/index.html`, puis
l'ajouter aux favoris Firefox pour ne plus avoir à écrire cette
commande.

On peut aussi créer un [raccourci
bureau](https://doc.ubuntu-fr.org/raccourci-lanceur) qui ouvrira la
documentation PostgreSQL dans firefox (ou tout autre navigateur en
modifiant la ligne de commande)

Placez la configuration suivante dans
`/usr/share/applications/postgresql-doc.desktop`, et vous obtiendrez
un lanceur qui ouvrira la documentation.

{{< highlight conf >}}
[Desktop Entry]
Name=Documentation PostgreSQL 13
Exec=firefox https://www.postgresql.org/docs/13/index.html
Icon=text-html
Type=Application
Categories=GTK;GNOME;Utility;
{{< /highlight >}}

On obtient le raccourci d'application suivant (exemple sous Gnome):

![Icône HTML de bureau Gnome](/desktop-postgresql-doc.jpg)

## Première connexion

Lors de l'installation de PostgreSQL, un premier utilisateur nommé
`postgres` a été créé. La méthode d'authentification de cette
utilisateur est
[`peer`](https://www.postgresql.org/docs/13/auth-peer.html).

Pour s'authentifier avec la méthode `peer`, vous devez vous
authentifier en tant qu'utilisateur du même nom dans votre système
d'exploitation, puis lancer le client PostgreSQL (`psql`): `sudo -u
postgres psql`

Vous voilà connecté en tant que `postgres`. Vous pouvez à présent
lancer des commandes SQL dans la console PostgreSQL, par exemple
`SELECT 1 + 1 as test;`

## Ajout d'un rôle et d'une base de données

Un rôle PostgreSQL est l'équivalent d'un utilisateur de PostgreSQL. On
peut [lister les rôles
existants](https://www.postgresql.org/docs/13/database-roles.html)
avec la méta commande `\du`. Pour l'instant, il existe uniquement le
rôle `postgresql` qui a les droits superutilisateur.

### Ajout d'un rôle pour notre base de données

On va créer un rôle ayant droit de modifier la structure/données de
notre base de données de test avec la commande [CREATE
ROLE](https://www.postgresql.org/docs/13/sql-createrole.html):
`CREATE ROLE role_tutoriel WITH LOGIN PASSWORD 'a_changer';`

On passe `LOGIN` en option pour pouvoir se connecter à la base de
données en tant qu'utilisateur `role_tutoriel`. On indique `PASSWORD`
suivi du mot de passe pour utiliser l'authentification par mot de
passe.

### Création d'une base de données

On va créer une base de données nommée `tutoriel_bdd` avec la commande
SQL [CREATE
DATABASE](https://www.postgresql.org/docs/13/manage-ag-createdb.html):
`CREATE DATABASE bdd_tutoriel OWNER role_tutoriel;`.

Cette nouvelle base de données appartient à `role_tutoriel`, qui a
donc le privilège de modifier cette base de données.

### Connexion à la base de données

On peut maintenant se connecter à la base de données avec le rôle
nouvellement créé. Dans un shell, utiliser la commande suivante: `psql
--password --host=localhost bdd_tutoriel role_tutoriel`

## Outils additionnels pour le développement avec PostgreSQL

### DBeaver

[DBeaver](TODO) est un client universel pour les bases de données
SQL. Il vous permettra d'écrire vos requêtes SQL avec de l'auto
complétion, de visualiser le schéma de votre base, ses données,
fonctions, modules, schémas, etc.

![Gestion de base de données avec
DBeaver](https://dbeaver.io/wp-content/uploads/2018/03/sql_script.png)

### Logs PostgreSQL

Le serveur PostgreSQL stocke son journal d'évènements dans le fichier
`/var/log/postgresql/postgresql-13-main.log`. Dans ce fichier, vous
pouvez retrouver entre autres:

+ Les connexions à la base de données
+ Les requêtes en erreur

## Conclusion

Créer une base de données PostgreSQL locale pour le développement est
rapide (trois commandes SQL suffisent) et permet de tester une
application dépendant fortement d'une base de données.
