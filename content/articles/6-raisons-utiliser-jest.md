---
title: "6 raisons de tester son code Javascript avec Jest"
date: 2022-01-24T7:30:00+01:00
tags: ["Javascript"]
type: "post"
description: >
  Jest est un framework de test Javascript populaire. Voici 6 raisons
  de tester son code avec Jest qui expliquent son succès.
image: "https://sagot.dev/jest-6-raisons.jpeg"
image_type: "summary"
---

Jest est un framework Javascript permettant de réaliser des tests
unitaires automatisés. Le test "unitaire" est le type de test qui
couvre la plus petite unité logique de votre code. Une unité peut être
par exemple une fonction, une classe ou une méthode de classe.

Voici 6 raisons d'utiliser Jest pour réaliser les tests unitaires de
son projet:

## Jest génère un rapport permettant de corriger rapidement son code

Jest fournit un [ensemble de
comparateurs](https://jestjs.io/fr/docs/using-matchers) qui vont
permettre d'expliquer pourquoi une comparaison n'a pas fonctionné, où
un test a échoué, ainsi de ce qui a fonctionné:

{{< highlight javascript>}}
test('Logarithme de 1 vaut zéro', () => {
  expect(Math.log(1)).toBe(0);
})

test('Logarithme est une fonction croissante', () => {
  expect(Math.log(12)).toBeLessThan(72);
})

test('Logarithme est la fonction inverse de l\'exponentielle', () => {
  expect(Math.log(Math.exp(48))).toBe(48);
})

test('Logarithme a une précision max de 1e-9', () => {
  expect(Math.log(50 + 1e-9)).toBe(Math.log(50 + 2e-9));
})
{{< /highlight>}}

![Exemple de rapport Jest](/jest-sortie.jpeg)

## Jest permet d'organiser ses tests de façon lisible

Le premier niveau d'organisation de vos tests est le système de
fichiers. Vous pouvez par exemple mettre côte à côte chaque fichier
contenant du code et un fichier contenant sa spécification. Le fichier
`src/utils.js` aura sa spécification `src/utils.spec.js` dans laquelle
se trouvent les tests Jest.

Vous pouvez également grouper vos tests par unité dans des blocs
`describe`:

{{< highlight javascript>}}
describe('La fonction logarithme', () => {
  it('vaut zéro en 1', () => {
    expect(Math.log(1)).toBe(0);
  });

  it('est croissante', () => {
    expect(Math.log(12)).toBeLessThan(72);
  });

  it('est la fonction inverse de l\'exponentielle', () => {
    expect(Math.log(Math.exp(48))).toBe(48);
  });
})
{{< /highlight>}}

![Exemple de sortie de Jest avec Describe](/jest-describe.jpeg)

## La documentation est complète, traduite et contient des exemples

La documentation de Jest est traduite dans 9 langages, dont le
français. Vous pourrez par exemple retrouver une [explication du
concept de mock et son
utilisation](https://jestjs.io/fr/docs/mock-functions).

Je vous conseille particulièrement la lecture de l'article
[premiers pas avec Jest](https://jestjs.io/fr/docs/getting-started)
ainsi que les suivants, qui présentent les fonctionnalités principales
de Jest.

Des guides sont aussi disponibles pour des projets
types. [Webpack est un exemple](https://jestjs.io/fr/docs/webpack).

## Vous pouvez adopter Jest progressivement

Vous pouvez vous lancer dans les tests unitaires avec Jest sans
configuration. Autrement dit, le coût initial d'adoption est faible.

Avec un simple `npm i --save-dev jest`, vous pouvez commencer à
utiliser Jest. La commande `npx jest` lancera Jest pour tous les
fichiers dont le nom contient `test`, `spec` ou est situé dans un
dossier `__test__`.

Un [tutoriel d'installation
rapide](https://jestjs.io/docs/getting-started) est disponible en
français.

Vous n'êtes bien sûr pas obligé de tout savoir avant de
commencer à implémenter des tests avec Jest. Commencez par des tests
simples, vous pourrez les améliorer plus tard au fur et à mesure que
votre compréhension des caractéristiques d'un bon test unitaires,
de Javascript et de Jest progresse.

Un premier test écrit avec Jest peut être aussi simple que cela:

{{< highlight javascript>}}
test('Logarithm of 1 is zero', () => {
  expect(Math.log(1)).toBe(0);
})
{{< /highlight>}}

Et le démarrage peut se faire en trois lignes de commandes:

![Installation et premier test Jest](/jest-single-test.jpeg)

## Jest permet d'isoler l'unité testée de son environnement

Pour un développeur, le test unitaire permet de s'assurer que
l'implémentation de l'unité testée répond aux spécifications données.

Un problème souvent rencontré lors des tests unitaires est qu'une
unité comporte souvent des **dépendances**. Une dépendance est une
autre partie du code utilisée par l'unité à tester. Par exemple, une
dépendance peut être un appel à une API, une fonction d'une librairie,
ou encore une fonction du projet.

Pour tester uniquement l'unité, et non pas l'unité et ses
dépendances, on peut remplacer le code dont dépend l'unité par une
implémentation sommaire.

Jest permet justement de controller les dépendances avec les
`mocks`. Un mock écrase le temps du test la définition d'une unité
pour la remplacer par une implémentation simpliste qui sera cohérente
dans le cadre du test. Par exemple, un appel à une API retournant une
donnée complexe pourra être remplacé par une simple fonction
retournant un résultat de test qu'on aura stocké dans un fichier du
projet.

## Jest est configurable et utilisable en ligne de commandes

Vous pouvez adapter la façon dont Jest va se comporter au niveau du
projet, dans un [fichier de
configuration](https://jestjs.io/fr/docs/configuration).

Vous pouvez par exemple configurer le parallélisme au niveau des
tests, la génération d'un rapport de couverture, l'utilisation de
Babel ou Typescript, etc.

Beaucoup des paramètres de la configuration de Jest sont modifiables
via des options en ligne de commandes. Vous pouvez lister les options
disponibles avec `npx jest --help`.

Les options que j'utilise le plus sont `-t` pour filtrer le nom des
tests à exécuter, `--watch` pour relancer les tests lorsqu'ils (ou les
fonctions qu'ils testent) sont modifiés. Il est aussi très pratique de
pouvoir passer en argument les noms des fichiers de tests à lancer,
par exemple `npx jest src/log.test.js`

## Conclusion

J'ai choisi d'utiliser Jest dans mes projets Javascript car ce
framework est facile d'accès et permet dès l'installation de fournir
les quelques fonctionnalités de base qu'on attend d'un framework de
test. A savoir des opérateurs de comparaison étendus et un outil de
rapport de test et de couverture.

Ce qui m'a ensuite séduit est une bonne documentation, des utilitaires
très utiles comme les mocks et une bonne capacité à être étendu et
configuré.

Et vous, quels sont les points clefs qui vous ont conduit à utiliser
Jest ? :point_down:
