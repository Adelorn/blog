---
title: "Matomo et RGPD: design d'une bannière de consentement"
date: 2022-02-19T11:00:00+01:00
tags: ["Javascript", "Projets"]
type: "post"
image: https://sagot.dev/dashboard-matomo.jpeg
image_type: "summary_large_image"
description: >
  Matomo est un outil d'analyse de traffic dont l'utilisation
  demande le consentement des utilisateurs. Réalisons un
  formulaire conforme au RGPD.
---

## Pourquoi utiliser Matomo

### Connaître ses visiteurs

Je n'ai installé [Matomo](https://fr.matomo.org/) sur mon serveur que
depuis une semaine, et il m'a déjà permis de:

- **Mesurer** la _conversion_ de mes sources de lecteurs
- **Retracer** le _parcours type_ d'un utilisateur sur mon site
- **Segmenter** les visites par _centres d'intérêts_ via les catégories
  d'articles
- **Analyser** les données clefs avec un dashboard personnalisé

Ces mesures m'ont permis de mieux comprendre ce qu'attendent les
visiteurs, et de mesurer ce que mes articles leur apportent.

![Dashboard Matomo personnalisé](/dashboard-matomo.jpeg)

### Collecter un minimum de données personnelles

Matomo permet de limiter la quantité de données collectées auprès de
ses utilisateurs, au point qu'il est possible d'utiliser Matomo [sans
collecter de données
personnelles](https://fr.matomo.org/cookie-consent-banners/).

Ce n'est pas le choix que j'ai fait, car je souhaite savoir si un
visiteur revient plusieurs fois sur mon blog.

Pour cela, un cookie contenant un identifiant unique est placé dans
votre navigateur. Ce cookie est une donnée personnelle car elle permet
de vous identifier uniquement sur mon instance de Matomo. Matomo me
permet de garder pour moi toutes les données collectées, qui ne sont
communiquées à aucun tiers.

### Instaurer une relation de confiance

Comme j'héberge moi-même mon instance de Matomo, je suis capable de
vous garantir:

- **Qui** aura accès à ces données (moi uniquement)
- Quelles sont les **données collectées** (usage du site, identifiant Matomo)
- Combien de **temps** ces données seront **conservées** (deux ans)
- À **quoi** servent ces données (mesure de l'intérêt des visiteurs)

### Rendre le pistage optionnel

Si vous ne souhaitez pas que vos visites soient enregistrées sur mon
instance Matomo, il suffit de l'indiquer une fois et aucune donnée ne
sera plus jamais communiquée (en dehors d'un unique signalement de
votre opposition au pistage à mon instance).

## Obligations du RGPD

Le [RGPD](https://www.cnil.fr/fr/rgpd-de-quoi-parle-t-on), Règlement
Général sur la Protection des Données, est une règlementation
Européene concernant la collecte de données personnelles.

Le RGPD définit une [donnée
personnelle](https://www.cnil.fr/fr/definition/donnee-personnelle)
comme **toute information se rapportant à une personne physique
identifiée ou identifiable**.

D'après le RGPD, avant toute collecte ou traitement d'une donnée
personnelle, l'utilisateur concerné doit consentir à ce traitement de
données, qui doit être clairement défini.

L'identifiant unique d'un utilisateur sur mon instance Matomo est une
donnée personnelle, je dois donc respecter le RGPD. Pour cela, je dois
demander aux utilisateurs si ils consentent à recevoir un cookie à des
fins de mesure d'audience, dont la portée est définie dans cet
article.

## Design d'une bannière adaptée à mon thème minimaliste

Pour demander aux utilisateurs si ils souhaitent participer à la
mesure d'audience de mon site, je vais ajouter une bannière en bas de
l'écran.

Comme [mon thème Hugo](https://github.com/526avijitgupta/gokarna) est
un thème minimaliste, je vais l'étendre avec une bannière minimaliste.

Cette nouvelle bannière aura une taille similaire à l'entête de mon
site. Le texte de demande sera à gauche, et à droite on trouvera deux
boutons pour indiquer le consentement, comme ci-dessous:

![Demande de consentement](/demande-consentement.jpeg)

### Réalisation du HTML

On commence par créer le morceau de document HTML qui contiendra la
bannière:

{{< highlight html >}}
<div id="consent-matomo">
    <div id="consent-matomo-text">
        Acceptez vous le dépôt d'un cookie de mesure d'audience ?
    </div>
    <div id="consent-button-container">
        <div class="consent-button" onclick="consentToMatomo()">Oui</div>
        <div class="consent-button" onclick="optOutToMatomo()">Non</div>
    </div>
</div>
{{< /highlight >}}

La bannière, `consent-matomo`, contient le conteneur du texte de la
demande, `consent-matomo-text`, ainsi que le conteneur des boutons
accepter/refuser, `consent-button-container`.

Lors d'un click sur un bouton, on exécutera soit `optOutToMatomo` qui
va désactiver le pistage, ou `consentToMatomo` qui va l'activer.

### Placement et style de la bannière

On passe maintenant au CSS. On réalise d'abord le conteneur, qui va
être placé en bas du document de manière fixe, et placer le texte à
gauche et les boutons à droite avec la disposition `flex`.

{{< highlight css >}}
#consent-matomo {
    /* Fixé en bas de l'écran */
    position: fixed;
    right: 0;
    left: 0;
    bottom: 0px;

    /* Padding  */
    padding: 20px 20px 20px 20px;

	/* Disposition des éléments fils */
	display: flex;
    justify-content: space-between;

    /* Utilisation des couleurs du thème Gokarna  */
    color: var(--light-text-color);
    background-color: rgba(var(--light-primary-color), 0.95);
}
{{< /highlight >}}

Le conteneur des boutons utilisera également `flex` pour placer les
deux boutons en ligne sur grand écran et passer en colonne lorsqu'il
n'y a plus de place (typiquement sur mobile).

{{< highlight css >}}
#consent-button-container {
    display: flex;
    flex-wrap: wrap;
}
{{< /highlight >}}

:fox: [Code HTML/CSS
complet](https://gitlab.com/Adelorn/gokarna/-/commit/a30f5bb6)

## Configuration JavaScript de Matomo

Maintenant qu'on a réalisé le design de notre bannière, il faut
implémenter les fonctions `optOutToMatomo` et `consentToMatomo`
appelées lors du clic sur un des deux boutons.

### Chargement du script JS Matomo

Pour utilser Matomo sur votre site, vous devez charger le script de
tracking de Matomo, disponible sur votre instance matomo à
`/matomo.js` par défaut. La fonction suivante va créer un élément
script d'id `matomo-tracking-script`, qui contient ce script de
tracking, puis le placer dans le document et le charger.

L'argument `trackerUrl` désigne la racine du domaine qui héberge
l'instance Matomo, qui est dans mon cas
`https://analytics.sagot.dev/`.

{{< highlight javascript >}}
function startMatomo(trackerUrl) {
    // Create a script node for Matomo
    let scriptNode = document.createElement('script');
    scriptNode.setAttribute('id', 'matomo-tracking-script');

    // Load the Matomo script
    let head = document.getElementsByTagName('head')[0];
    scriptNode.async = true;
    scriptNode.src = trackerUrl + 'matomo.js';
    head.appendChild(scriptNode);
}
{{< /highlight >}}

### Configuration du traceur Matomo avec consentement requis

Après avoir chargé le script de Matomo, on va le configurer, en
indiquant que le consentement est requis, ainsi que les
fonctionnalités que l'on veut activer.

{{< highlight javascript >}}
function configureMatomo(trackerUrl) {
    // Fetch matomo config
    var _paq = window._paq = window._paq || [];

    // Require user consent before sending any personnal data to our
    // tracking server
    _paq.push(['requireConsent']);

    // Enable some Matomo features
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    _paq.push(['enableHeartBeatTimer']);

    // Set Matomo url and site id
    _paq.push(['setTrackerUrl', trackerUrl + 'matomo.php']);
    _paq.push(['setSiteId', '1']);
}
{{< /highlight >}}

### Recueil du consentement de l'utilisateur

Maintenant que Matomo peut être chargé et configuré, on va pouvoir lui
indiquer si l'utilisateur a donné son consentement ou non:

{{< highlight javascript >}}
function askConsent() {
    let consentNode = document.getElementById('consent-matomo');
    consentNode.style.display = 'flex';
}

function closeAskConsent() {
    let consentNode = document.getElementById('consent-matomo');
    consentNode.style.display = 'none';
}

function consentToMatomo() {
    closeAskConsent();

    // Ask Matomo to remember consent
    _paq.push(['rememberConsentGiven']);
    _paq.push(['trackEvent', 'Tracking policy updated', 'Consent']);
}

function optOutToMatomo() {
    closeAskConsent();
    setCookie('optout', 't', 365); // Opt out for a year

    // Send an Opt Out event to the server
    _paq.push(['setConsentGiven']);
    _paq.push(['trackEvent', 'Tracking policy updated', 'Opt out']);
    _paq.push(['forgetConsentGiven']);
}
{{< /highlight >}}

### Finalisation et intégration du script JS

Lors du chargement de la page, on va charger et configurer Matomo si
l'utilisateur n'a pas opposé son consentement, et lui demander son
consentement si il ne s'est pas encore prononcé:

{{< highlight javascript >}}
window.addEventListener('load', () => {
    let userOptedOut = getCookie('optout') != null;
    if (userOptedOut) {
		return;
    }

    let matomoHostedAt = "https://analytics.sagot.dev/";
    configureMatomo(matomoHostedAt);
    startMatomo(matomoHostedAt);

    let consentGiven = getCookie('mtm_consent') != null;
    if (!consentGiven) {
		askConsent();
    }
})
{{< /highlight >}}

:fox: [Le code JS est également sur
Gitlab](https://gitlab.com/Adelorn/gokarna/-/commit/a30f5bb6)

## Bonus: détection de bloqueurs de publicités

Un problème subsiste: un utilisateur peut accepter la mesure
d'audience, mais la bloquer avec un bloqueur de publicité par
inadvertance.

Lorsqu'un bloqueur de publicité est utilisé, le chargement du script
`matomo-tracking-script` va être bloqué. Dans ce cas, l'évènement
`error` sera appelé sur ce script.

Lorsque ce script est bloqué, on va simplement ajouter au message
qu'il faut désactiver le bloqueur de publicité pour participer aux
mesures d'audience:

{{< highlight javascript >}}
function noticeBlockerInConsent() {
    let consentTextNode = document.getElementById('consent-matomo-text');
    consentTextNode.textContent += ' Si oui, merci de désactiver votre bloqueur.';
}
{{< /highlight >}}

{{< highlight javascript "hl_lines=10" >}}
function startMatomo(trackerUrl) {
    // Create a script node for Matomo
    let scriptNode = document.createElement('script');
    scriptNode.setAttribute('id', 'matomo-tracking-script');

    // Load the Matomo script
    let head = document.getElementsByTagName('head')[0];
    scriptNode.async = true;
    scriptNode.src = trackerUrl + 'matomo.js';
    scriptNode.onerror = noticeBlockerInConsent; // Ask blocker removal
    head.appendChild(scriptNode);
}
{{< /highlight >}}

:sparkles: [feat(analytics): Ask for blocker removal if consent to
tracking](https://gitlab.com/Adelorn/gokarna/-/commit/a85aa208)

## Conclusion

Matomo est un outil puissant qui permet de comprendre ses utilisateurs
tout en conservant leur confiance grâce à l'auto-hébergement et à un
traceur unique, audité et sous contrôle du propriétaire du site web.

Ces qualités ne l'exemptent pas toujours de demander le consentement
aux utilisateurs du site, ce qui peut être fait simplement et sans
nuire à l'expérience utilisateur comme on l'a vu dans cet article.

Et si on faisait une compétition de dashboards Matomo en commentaires
? :point_down:
