---
title: "Éditer soi-même ses factures Auto-Entrepreneur avec LaTeX"
date: 2021-12-26T18:15:00+02:00
lastmod: 2023-01-11T08:00:00+0100
tags: ["trucs-et-astuces"]
type: "post"
image: "/modele-facture.jpeg"
description: >
  LaTeX permet de réaliser des documents de haute qualité. Dans cet article,
  je présente comment utiliser ce logiciel pour faire un modèle de facture.
---

De nombreux sites comme [facture.net](https://facture.net) permettent
d'éditer facilement ses factures. Mais cette approche limite la
personnalisation de l'apparence et du contenu de ces factures.

Je vous propose donc d'utiliser à la place
[LaTeX](https://www.latex-project.org/), un outil qui permet de
réaliser des documents techniques/scientifiques de haute qualité.

Nous allons réaliser un modèle LaTeX de facture donnant le résultat suivant:

![Modèle de facture LaTeX](/modele-facture.jpeg)

## Préambule, format et marges du document

Un document LaTeX est composé de deux parties:

+ Le préambule
+ Le corps du document

Dans le préambule, nous allons déclarer quels modules utiliser dans
notre modèle de facture, ainsi que des paramètres globaux du document
LaTeX comme le format (A4 dans notre cas).

Le corps du document contiendra le contenu de la facture, à savoir son
émetteur, destinataire, la liste des services réalisés et le montant
total.

Voici le document LaTeX de base sur lequel nous allons travailler:

{{< highlight tex >}}
% Le document est au format A4 avec des marges de 2 cm à gauche
% et à droite, 1.5 cm en haut et 3 cm en bas
% Le paquet geometry permet de définir le format du document et ces marges
\usepackage[a4paper, left=2cm, right=2cm, top=1.5cm, bottom=3cm]{geometry}

% Ajoutons le paquet xcolor qui nous permet d'ajouter des couleurs à notre
% tableau de services facturés au client
\usepackage[table]{xcolor}

% Cette commande permet de supprimer l'indentation en début de
% paragraphe qui n'a pas de sens dans une facture qui n'en contient pas
\setlength\parindent{0cm}

% Le corps (contenu) de notre modèle de facture est contenu
% dans l'environnement document contenu entre les deux balises
% \begin{document} et \end{document}
\begin{document}

\end{document}
{{< /highlight >}}

## Titre et date de la facture

La première chose que nous allons ajouter au contenu du modèle de notre facture est son titre et sa date:

{{< highlight tex "hl_lines=3-9">}}
\begin{document}

{ % Début de l'environnement LaTeX
  \Large
  \textbf{Facture n°20210001} % En gras

  \vspace{.2cm} % Espace vertical de 0.2 cm
  26 décembre 2021
} % Fin de l'environnement LaTeX

\end{document}
{{< /highlight >}}

Je souhaite que la taille du titre de la facture ainsi que sa date
soient plus large que le reste du document. Pour cela, je place ce
titre et cette date dans un environnement LaTeX, c'est à dire entre un
crochet ouvrant `{` et un crochet fermant `}`. Les commandes LaTeX
exécutées dans cet environnement ne concernent que le contenu qui s'y
trouve (ici le titre et la date). Ainsi, la commande `\Large` qui
augmente la taille du texte ne s'applique qu'à ces deux éléments.

J'ai également mis le titre en gras avec la commande `\textbf` et
espacé le titre et la date de 0.2 cm supplémentaires avec `\vspace`.

## Liste des services et produits vendus au client

Ajoutons maintenant l'objet principal de la facture, à savoir
l'ensemble des services et produits que vous avez fourni à votre
client.

Nous allons afficher un service/produit par ligne du tableau des
services rendus:

{{< highlight tex >}}
{\large \textbf{Détail}} % Détail des services/produits facturés

% La commande suivante applique aux lignes du tableau une alternance
% entre deux gris clairs et foncés
\rowcolors{2}{gray!10}{gray!40}
% Le deuxième environnement en argument du tableau indique
% que la première colonne du tableau est alignée à gauche (l), la deuxième
% est un paragraphe (p) de 12cm de large (p{12cm}) et la dernière colonne
% est également alignée à gauche (l)
\begin{tabular}{lp{10cm}lll}
  Type & Description & Prix HT & TVA & Prix TTC \\
  \hline % Trait de séparation horizontal
  Produit & Armoire & 750 \euro & 20\% & 900 \euro \\ % Produit vendu au client
  % Service vendu au client
  Service & Livraison, déménagement et assemblage de l'armoire & 200 \euro  & 20\% & 140 \euro \\
  \hline % Trait de séparation horizontal
\end{tabular}
{{< /highlight >}}

Dans les données d'exemple du modèle de facture, il y a un produit et
un service. Dans notre tableau, on trouve trois lignes. La première
est l'entête (Type, Description, Prix HT), la seconde le produit vendu
et la dernière le service rendu. Chaque ligne est séparée de la
suivante par un retour à la ligne `\\`. Chaque colonne est séparée de
la colonne suivante par `&`.

La commande `\hline` permet d'ajouter un trait noir horizontal au
tableau. La commande `\texteuro` insère un symbole €.

## Total de la facture et conditions

On ajoute maintenant le total, ici 950€ à la facture, ainsi que les
conditions de paiement.

{{< highlight tex >}}
\vspace{.5cm}
{\footnotesize TVA non applicable, art. 293 B du CGI}

\vspace{.1cm}
\textbf{Total HT}: 950 \euro \\
\textbf{Total TTC}: 1040 \euro

\vspace{.5cm}
{\large \textbf{Conditions}}

\vspace{.3cm}
Conditions de paiement: 45 jours date de facture
{{< /highlight >}}

## Émetteur et destinataire de la facture

Il manque un dernier élément au modèle de facture: son émetteur et son
destinataire.

Dans notre facture, nous allons mettre deux tableaux côtes à
côtes. Celui de gauche contiendra les coordonnées de notre entreprise
et celui de droite celui du client.

Pour obtenir cet effet, nous allons utiliser deux environments LaTeX
`minipage` qui contiennent chacun un tableau. Je ne vais pas aborder
le fonctionnement de cet environnement dans l'article, mais je vous
conseille cette [explication de
minipage](https://latex-tutorial.com/minipage-latex/) pour rentrer
dans les détails. Pour ce tutoriel, retenez que
cela peut permettre de placer deux tableaux (ou tout autre contenu)
sur une même ligne.

{{< highlight tex "hl_lines=11-36">}}
\begin{document}

{
  \Large
  \textbf{Facture n°20210001}

  \vspace{.2cm}
  26 décembre 2021
}

\begin{minipage}[t]{9.5cm} % Partie émetteur à gauche
  {\large \textbf{Émetteur}}

  \vspace{.3cm}
  \begin{tabular}{@{}ll@{\vspace{.07cm}}}
    {\small Société:} & \textbf{VOTRE SOCIÉTÉ} \\
    {\small SIREN:} & 552178639 \\
    {\small Numéro TVA:} & FR42552178639 \\
    {\small Contact:} & Votre nom \\
    {\small Courriel:} & votre.mail@exemple.fr \\
    {\small Téléphone:} & +33 7 00 11 22 33 \\
    {\small Adresse:} & 5 rue du Phare \\
     & 44000 Nantes
  \end{tabular}
\end{minipage}
\begin{minipage}[t]{7.5cm} % Partie destinataire à droite
  {\large \textbf{Destinataire}}

  \vspace{.3cm}
  \begin{tabular}{@{}ll@{\vspace{.07cm}}}
    {\small Société:} & \textbf{VOTRE SOCIÉTÉ CLIENT} \\
    {\small Contact:} & Référent client \\
    {\small Courriel:} & mail.client@exemple.fr \\
    {\small Adresse:} & 1 Parvis de la Loire \\
     & 95015 Paris
  \end{tabular}
\end{minipage}
{{< /highlight >}}

## Mentions obligatoires d'une facture

Le gouvernement a établi une [liste des mentions
obligatoires](https://www.economie.gouv.fr/entreprises/factures-mentions-obligatoires)
devant apparaître sur votre facture. L'exemple étudié dans cet article
devrait comporter ces mentions obligatoires, mais je vous conseille de
consulter cette liste afin que vous soyez sûr d'être en règle.

Si vous n'êtes pas [redevable de la
TVA](https://www.portail-autoentrepreneur.fr/academie/statut-auto-entrepreneur/tva),
ajoutez la mention `TVA non applicable, art. 293 B du CGI` à la fin de
votre document (cf section `mentions particulières` de la [liste des
mentions](https://www.economie.gouv.fr/entreprises/factures-mentions-obligatoires))

## Résultat

Vous pouvez maintenant télécharger et utiliser le [modèle LaTeX de
facture](/modele-facture.tex). Pour transformer ce fichier LaTeX en
PDF puis en facture papier, vous aurez besoin [d'installer
LaTeX](https://www.latex-project.org/get/) qui est un logiciel libre
(et donc gratuit).
