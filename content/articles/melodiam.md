---
title: "Comment j'ai créé un site de streaming local"
date: 2021-10-26T19:15:00+02:00
tags: ["projets"]
type: "post"
image: "/melodiam.webp"
---

## L'idée

En juillet 2020, j'ai voulu créer une plateforme qui permettrait de
découvrir facilement les artistes d'une ville. Sur ce site, un
utilisateur pourrait sélectionner sa ville et écouter une mixtape d'un
artiste local qui lui aurait tapé dans l'oeil.

J'ai créé le projet avec deux étudiants,
[Antoine Garban](https://www.linkedin.com/in/antoine-g-4b5ba5133/)
en école de commerce et
[Mélanie Piat](https://www.facebook.com/melanie.estevadubrule)
étudiante aux beaux-arts de Lyon. Mélanie est d'ailleurs membre du
groupe
[Plus Plus Plus](https://melodiam.fr/artistes/plus-plus-plus).

Antoine était responsable de la communication, Mélanie du design du
site et moi de son développement. Nous échangions également
régulièrement sur les fonctionnalités souhaitées pour le site que nous
avons appelé [Melodiam](https://melodiam.fr/).

[![Melodiam](/melodiam.webp)](https://melodiam.fr/)

Le résultat et aujourd'hui
[disponible en ligne](https://melodiam.fr/)
et une dizaine
[d'artistes Grenoblois](https://melodiam.fr/villes/grenoble)
sont référencés sur
[Melodiam](https://melodiam.fr/).

## Développement de Melodiam

### Architecture du projet

Le projet est découpé en trois parties:

- Interface web: [Client](https://melodiam.fr/)
- Accès aux données: [API](https://api.melodiam.fr/explorer)
- Distribution des musiques: [CDN](https://cdn.melodiam.fr/)

L'interface est le point d'entrée de l'utilisateur. Cette interface
contient la logique de Melodiam, mais pas les données des artistes qui
y sont inscrits. Pour obtenir les données de ces artistes, l'interface
fait appel à l'API. Pour obtenir les musiques des artistes locaux, c'est le CDN
([content delivery network](https://en.wikipedia.org/wiki/Content_delivery_network))
qui se charge de transmettre le contenu.

### Interface

Nous avons choisi de réaliser l'interface sous forme d'une
["single page application"](https://en.wikipedia.org/wiki/Single-page_application)
réalisée avec
[Vue 2](https://vuejs.org/).
Cette structure d'application permet de passer d'une page à une autre
sans chargement et donc une navigation fluide pour l'utilisateur.

L'objectif de Melodiam est de permettre à l'utilisateur de découvrir
de nouvelles musiques. L'utilisation de Vue m'a permis de ne pas
interrompre la lecture de la musique courante lors d'un changement de
page.

#### Lecteur de musique

![Lecteur musical de Melodiam](/melodiam_player.webp)

Voici l'implémentation du design réalisé par Mélanie du lecteur de musique.

La partie complexe du développement est la barre affichant le progrès
de la musique courante, car l'utilisateur peut avancer/reculer le
temps de la musique courante avec un glisser/déposer.

Je vous suggère d'écouter un son sur [Melodiam](https://melodiam.fr/)
avant de voir comment je l'ai implémenté :wink:

Comme on veut limiter le mouvement du bouton orange à l'axe X, j'ai
choisi de réagir à l'évènement click au lieu d'utiliser
l'[API drag and drop](https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API).

Du point de vue HTML, on utilise une barre vide contenant une barre de
remplissage et un bouton déplaçable permettant de changer le temps de
la musique. Le bouton et la barre d'écoute ont une
[classe CSS dynamique](https://vuejs.org/v2/guide/class-and-style.html)
reliée au code du
[composant Vue](https://fr.vuejs.org/v2/guide/components.html).

{{< highlight html >}}
<!-- Dragable progress bar -->
<div class="empty-bar">
  <div
	ref="barreTempsEcoute"
	class="filling-bar"
	:style="fillingStyle"
  />

  <!-- Watch the mouse pressed event on the button -->
  <div
	class="button"
	@mousedown="drag"
  />
</div>
{{< /highlight >}}

Voici le code javascript associé au composant. Ce code permet de
mettre à jour le remplissage de la barre de progrès quand
l'utilisateur.

{{< highlight js >}}
data () {
	return { dragging: false, filling: 0 };
},
computed: {
	fillingStyle () {
		return { width: `${this.filling}px`};
	}
},
methods: {
	drag () {
		window.addEventListener('mousemove', this.updateFilling);
	},
}
{{< /highlight >}}

Il reste à implémenter la fonction updateFilling qui met à jour le
remplissage de la barre de progrès de la musique en fonction du
déplacement de la souris de l'utilisateur.

{{< highlight js >}}
methods: {
	updateFilling(event) {
		// Get where the user has dragged the button
		let barOffset = event.clientX - this.$refs.barreTempsEcoute.offsetLeft;

		// The new filling (in percent) corresponds to
		// the position of the button relatively to the bar width
		let barWidth = this.$refs.barreTempsEcoute.clientWidth;
		let newFilling = Math.round(barOffset / barWidth);

	    // The user could have draged the button further than the bar width
		// or before the bar beginning
		newFilling = Math.min(100, newFilling);
		newFilling = Math.max(0, newFilling);

	    // Update the filling
		this.filling = newFilling;
	}
}
{{< /highlight >}}

On a finalement une barre de progrès de la musique réactive lors d'un
drag and drop sur l'axe X. J'ai omis quelques détails pour que la démo reste concise :smiley:

### API

J'ai réalisé l'API en
[TypeScript](https://www.typescriptlang.org/).
Ce langage est une surcouche du
[Javascript](https://developer.mozilla.org/fr/docs/Learn/JavaScript/First_steps/What_is_JavaScript).
Cela me permet de rester efficace quand je bascule du développement de
l'API à celui de l'interface (car je n'ai pas à changer de langage de
programmation).

Côté librairies, j'ai utilisé le framework
[LoopBack 4](https://loopback.io/index.html)
qui permet de réaliser rapidement des APIs et comporte une
[interface en ligne de commande](https://loopback.io/doc/en/lb4/Command-line-interface.html)
permettant de générer les requêtes
[CRUD](https://fr.wikipedia.org/wiki/CRUD)
basiques de l'application.

[![API](/melodiam_api.webp)](https://api.melodiam.fr/explorer)

LoopBack permet également de générer une documentation OpenAPI, visible ci-dessus.

En plus du CRUD, j'ai implémenté l'authentification par mail/mot
de passe et une autorisation de récupérer les données.

Concernant l'autorisation, un utilisateur est autorisé à récupérer une
donnée si il l'a générée (ex: informations du compte) ou si elle est
publique (ex: artistes).

L'API permet au final de réaliser les opérations suivantes:
- Création d'un compte
- Authentification
- Enregistrement de son profil artiste
- Publication de sa mixtape (collection de musiques)
- Aimer un profil d'artiste

### CDN

Un CDN est normalement composé d'un réseau d'ordinateurs. Dans notre
cas, un seul ordinateur est dans ce réseau, celui hébergeant le
site. Le CDN désigne dans notre cas le serveur chargé de distribuer
les musiques.

L'API et le CDN fonctionnent en production dans deux conteneurs
[Docker](https://www.docker.com/)
différents partageant un même dossier de musiques. Le CDN y a accès en
lecture seule et l'API écrit les musiques dedans lors d'une mise en
ligne par un artiste.

{{< highlight yml >}}
services:
  api:
    image: melodiam/api:0.4.0
	volumes:
		- /images/on/host:/app/.uploads

  cdn:
    image: nginx:1.19
	volumes:
		- /images/on/host:/usr/share/nginx/html:ro
{{< /highlight >}}

## Fin?

Développer Melodiam avec Mélanie et Antoine était une très belle
expérience. Le projet, qui est en ligne, est fonctionnel. Melodiam a
retenu l'intérêt des artistes locaux de Grenoble. En revanche, il y
a peu de trafic organique, autrement dit peu d'internautes
recherchent "artistes de Grenoble" sur leur moteur de recherche.

Comme le but de notre site était de répondre à cette recherche qui
semble rencontrer une faible demande, nous avons mis le développement en
pause pour le moment.

Mais qui sait, Melodiam trouvera peut être de l'engouement dans les
années à venir :crossed_fingers:
