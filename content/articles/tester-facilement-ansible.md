---
title: "Tester facilement un rôle Ansible avec Docker"
date: 2021-12-25T12:30:00+02:00
tags: ["ansible"]
type: "post"
description: >
  Pour tester un rôle Ansible, pas besoin de
  machine virtuelle ou de serveurs. Découvez comment
  réaliser simplement vos tests avec Docker
---

La connexion aux machines distantes se fait généralement en `ssh`.
C'est aussi le moyen de connexion qu'utilise Ansible, mais saviez-vous
que d'autres types de connexions sont supportés ? Dans ce tutoriel,
nous verrons comment utiliser une connexion locale à un conteneur
Docker pour vous permettre de tester facilement un rôle Ansible sur
votre machine.

## Prérequis

Pour ce tutoriel, il est nécessaire d'avoir [installé
Ansible](/articles/installer-ansible-ubuntu/) ansi que
Docker. Rendez-vous sur [mon tutoriel d'installation avec
Pip](/articles/installer-ansible-ubuntu/) pour Ansible. Pour Docker:

Voici comment installer Docker sur Linux avec le script
officiel. Prenez le temps de regarder la source avant d'exécuter le
script téléchargé avec `curl`.

{{< highlight sh>}}
# Téléchargement du script d'installation de Docker
curl -fsSL https://get.docker.com -o get-docker.sh

# Execution du script (en tant que root)
sudo sh get-docker.sh
{{</ highlight >}}

## Conteneur Docker Debian compatible avec Ansible

### Spécification de l'image Docker

On utilise une image Docker de la distribution Debian pour réaliser
nos tests. L'image de base disponible sur le [Docker
Hub](https://hub.docker.com/_/debian/) ne contient pas Python 3 dont
Ansible a besoin pour exécuter votre rôle.

Pour rendre cette image compatible avec Ansible, on va donc hériter de
l'image Debian de base et installer Python 3. Pour cela, on utilise le
`Dockerfile` suivant:

{{< highlight Dockerfile >}}
FROM debian:bullseye
RUN apt update && apt install -y python3 python3-pip
CMD tail --follow /dev/null
{{</ highlight >}}

**Astuce**: on ajoute également la commande par défaut `tail --follow
/dev/null` à l'image du conteneur. Cela a pour effet d'attendre
passivement l'insertion de données dans `/dev/null`. Comme cet
évènement n'arrive jamais, le conteneur sera toujours en attente
passive.

### Contruction de l'image

On va construire l'image décrite dans le `Dockerfile` et lui donner un
nom. Assurez-vous d'avoir placé le `Dockerfile` dans le dossier
courant de votre shell.

{{< highlight sh >}}
docker build --tag=debian:bullseye-python .
{{</ highlight >}}

Notre image Docker `debian:bullseye-python` est maintenant prête à
l'emploi pour être lancée dans un conteneur qu'on utilisera comme
machine de test pour notre rôle.

### Instantiation du conteneur

On lance une instance de l'image précédemment construite
`debian:bullseye-python` avec la commande:

{{< highlight sh >}}
docker run --detach --name=conteneur_test debian:bullseye-python
{{</ highlight >}}

Voilà, notre conteneur nommé `conteneur_test` est prêt pour tester
votre rôle Ansible.

## Utilisation du conteneur de test pour tester un rôle

### Inventaire

On ajoute maintenant notre conteneur de test à l'[inventaire
Ansible](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html).
Pour cela, on précise à Ansible qu'il ne devra pas se connecter en ssh
mais en utilisant le démon Docker local. On édite pour cela le fichier
hosts de votre répertoire courant, avec l'option
`ansible_connection=docker` qui indique à Ansible qu'il devra se
connecter via Docker.

{{< highlight conf >}}
[serveur_debian_test]
conteneur_test ansible_connection=docker ansible_user=root
{{</ highlight >}}

J'ai nommé le groupe de serveur `serveur_debian_test` pour le
distinguer de vos serveurs réels.

### Test du rôle

Pour simplifier, nous allons lancer un [module
ad-hoc](https://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html)
sur notre conteneur de test. Cela équivaut à lancer un rôle sur
le conteneur de test.

Testons dans un premier temps que le conteneur de test est bien
atteignable par Ansible avec son module ping:

{{< highlight sh >}}
# Test de connectivité via Ansible
ansible --inventory hosts --module-name ping serveur_debian_test
# résultat:
# conteneur_test | SUCCESS => {
#     "ansible_facts": {
#         "discovered_interpreter_python": "/usr/bin/python3"
#     },
#     "changed": false,
#     "ping": "pong"
# }
{{</ highlight >}}

Le rôle/module que nous allons maintenant tester est l'installation de
Python3 sur une machine distante. Comme on a déjà installé ce logiciel
sur notre machine de test, le résultat devrait retourner
`changed=false`.

{{< highlight sh >}}
# Vérification de l'installation de Python3
ansible --inventory hosts --module-name apt \
  --args "name=python3 state=present" \
  serveur_debian_test
# résultat:
# conteneur_test | SUCCESS => {
#     "ansible_facts": {
#         "discovered_interpreter_python": "/usr/bin/python3"
#     },
#     "cache_update_time": 1640430554,
#     "cache_updated": false,
#     "changed": false
# }
{{</ highlight >}}

On obtient bien le résultat attendu du rôle `apt`, à savoir que
Python3 est déjà installé (`changed=false`).

## Bonus: script automatisé

Je mets à disposition un [script Bash](/ansible/conteneur-test.sh) qui
réalise une synthèse de cet article. Ce script permet de vérifier
votre installation de Docker, de construire l'image de test et de
recréer le conteneur de test si il est déjà lancé.

## Conclusion

Vous avez appris à utiliser un conteneur local pour tester vos rôles
Ansible en toute simplicité. Cette approche de test est la base de
frameworks plus complexes comme
[Molecule](https://molecule.readthedocs.io/en/latest/index.html).

Pour industrialiser ce processus de test, retrouvez comment [tester
vos rôles Ansible avec Molecule](/articles/roles-ansible).
