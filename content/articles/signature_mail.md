---
title: "Comment créer sa signature mail en HTML"
date: 2021-11-11T09:15:00+01:00
tags: ["trucs et astuces"]
type: "post"
image: "/carte_visite_recto_html5.webp"
description: "Créez votre signature mail simple avec HTML. Vous apprendrez HTML en même temps!"
---

![Carte de visite de Jules Sagot](/carte_visite_recto.webp) 

Voici ma [carte de visite](https://sagot.dev/carte_visite.pdf). C'est
une image. Le fil rouge de cet article est la production d'un document
[HTML](https://html.com/) qui donnera le même visuel que cette image
dans un navigateur. Une fois ce document HTML produit, vous pourrez
l'inclure comme signature de vos mails pro 👍

## Prérequis

Nous allons éditer un fichier HTML. Pour ce faire, vous aurez besoin
d'un éditeur de texte. Personnellement, j'utilise et recommande [GNU
Emacs](https://www.gnu.org/software/emacs/) mais
[Notepad++](https://notepad-plus-plus.org/) (Windows) ou Gedit
([Linux](https://doc.ubuntu-fr.org/gedit)) seront plus faciles à
utiliser. Pour voir le résultat, il faudra ouvrir le fichier html
édité dans un navigateur web
([Firefox](https://www.mozilla.org/fr/firefox/new/) par exemple).

## Création d'un fichier HTML

Avec votre éditeur de texte, créez un fichier nommé
`signature.html`. Utilisez le texte suivant pour avoir le début de
la carte de visite.

{{< highlight html >}}
<html>
    <head>
        <title>Signature mail de Jules Sagot--Gentil</title>
        <meta charset="utf-8" />
    </head>
    <body>
        <div>
            <div>Jules SAGOT</div>
        </div>
    </body>
</html>
{{< /highlight >}}

Sauvegardez le fichier et ouvrez le avec votre navigateur web.

![Premier résultat de la carte de visite en HTML](/signature_mail/first_step.webp)

Voici votre premier document HTML! Remplacez maintenant Jules Sagot par votre nom.

## Quel est le sens du texte HTML que j'ai copié/collé ?

Le langage HTML est un assemblage de balises. Prenons l'exemple de la
balise `head` que nous avons utilisé dans le code HTML. Une balise
peut être ouvrante: `<head>` ou fermante: `</head>`. Ces balises
peuvent en contenir d'autres. Dans notre carte de visite, la balise
`head` (l'entête du document HTML) en contient deux autres: `title`
dont le contenu est le titre du document et `meta` qui indique
l'encodage du document (ici `utf-8`).

Pour afficher du contenu sur la page web, il faut insérer des balises
entre la balise `body` ouvrante et la balise `body` fermante. Pour
l'instant, le corps du document (le `body`) contient une seul balise
avec votre nom.

## Ajoutons vos coordonnées au fichier HTML

Nous allons ajouter une série de balises `div` contenant les autres
informations de la carte de visite, par exemple votre métier
(Développeur Fullstack pour ma part).

{{< highlight html >}}
<html>
    <head>
        <title>Signature mail de Jules Sagot--Gentil</title>
        <meta charset="utf-8"></meta>
    </head>
    <body>
        <div>
            <div>Jules SAGOT</div>
            <div>Développeur Fullstack</div>
            <div>Tel: +33 (0)7 81 89 87 41</div>
            <div>Courriel: jules@sagot.dev</div>
            <div>Blog: sagot.dev</div>
            <div>44980 Sainte Luce sur Loire</div>
        </div>
    </body>
</html>
{{< /highlight >}}

On obtient le résultat suivant après actualisation de la page web dans
le navigateur (touche F5):

![Deuxième résultat de la carte de visite en HTML](/signature_mail/second_step.webp)

## Ajout d'un logo à la carte de visite

Un fichier HTML peut contenir des références vers des images à
afficher dans le document. C'est ce que l'on va faire pour ajouter
votre logo. Je vais utiliser [mon
logo](https://sagot.dev/js_logo_rond.svg) pour l'exemple. L'image doit
être située sur un serveur distant (vous avez
sûrement le logo de votre entreprise dispo sur le web).

Pour inclure la référence à l'image dans le document, nous allons
utiliser la balise `img` (balise image). Cela aura pour effet
d'inclure le logo dans le rendu de la page HTML.

{{< highlight html >}}
<html>
    <head>
        <title>Signature mail de Jules Sagot--Gentil</title>
        <meta charset="utf-8"></meta>
    </head>
    <body>
        <div>
            <img src="https://sagot.dev/js_logo_rond.svg" alt="Logo Jules Sagot--Gentil"></img>
            <div>Jules SAGOT</div>
            <div>Développeur Fullstack</div>
            <div>Tel: +33 (0)7 81 89 87 41</div>
            <div>Courriel: jules@sagot.dev</div>
            <div>Blog: sagot.dev</div>
            <div>44980 Sainte Luce sur Loire</div>
        </div>
    </body>
</html>
{{< /highlight >}}

On ajoute un argument à la balise img: `src`. Sa valeur est l'endroit
(le serveur web sagot.dev dans l'exemple) où est sauvegardé le logo à
inclure dans la page HTML.

On voit le logo apparaître sur la page web:

![Troisième résultat de la carte de visite en HTML](/signature_mail/third_step.webp)

Le logo est trop grand! Mais nous allons changer ça. On va rajouter
une balise `style` à la carte de visite. Cela va permettre de préciser
la taille du logo et sa position. Nous allons utiliser le langage
[CSS](https://html.com/css/#What_is_CSS) pour cela. [HTML](https://html.com/)
décrit la structure du document, et CSS la façon d'afficher les
éléments de cette structure.

## Modification de la taille et position du logo

On veut placer le logo à droite des informations me concernant. Pour
cela, on va ajouter un code CSS dans une balise `style`.

Le style suivant indique que toutes les balises `img` du document
doivent faire une taille de 6rem (rem est une unité de mesure, passer
de 6rem à 12rem augmente la taille de l'image). On place également
l'image hors du flot normal du document avec le paramètre `position`
et on place l'image à 18rem de la gauche (paramètre `left`) de son
parent qui est `body`.

{{< highlight css >}}
img {
    position: absolute;
    left: 18rem;
    width: 6rem;
}
{{< /highlight >}}

On ajoute ce style au document à l'intérieur d'une balise `style` elle
même située dans la balise `head`:

{{< highlight html >}}
<html>
    <head>
        <title>Signature mail de Jules Sagot--Gentil</title>
        <meta charset="utf-8"></meta>
        <style>
            img {
                position: absolute;
                left: 18rem;
                width: 6rem;
            }
        </style>
    </head>
    <body>
        <div>
            <img src="https://sagot.dev/js_logo_rond.svg" alt="Logo Jules Sagot--Gentil"></img>
            <div>Jules SAGOT</div>
            <div>Développeur Fullstack</div>
            <div>Tel: +33 (0)7 81 89 87 41</div>
            <div>Courriel: jules@sagot.dev</div>
            <div>Blog: sagot.dev</div>
            <div>44980 Sainte Luce sur Loire</div>
        </div>
    </body>
</html>
{{< /highlight >}}

On voit maintenant que le logo est à la bonne taille et que le texte
n'est pas impacté par sa présence car il est en `position: absolute`.

![Quatrième résultat de la carte de visite en HTML](/signature_mail/fourth_step.webp)

## Ajout de liens vers le site/téléphone/courriel

Pour l'instant, mes informations de contact sont sous la forme de
texte uniquement. Nous allons ajouter des liens (balise `a`) pour que
l'utilisateur qui reçoit la carte de visite puisse cliquer dessus pour
envoyer un mail ou nous appeler.

{{< highlight html >}}
<html>
    <head>
        <title>Signature mail de Jules Sagot--Gentil</title>
        <meta charset="utf-8"></meta>
        <style>
            img {
                position: absolute;
                left: 18rem;
                width: 6rem;
            }
        </style>
    </head>
    <body>
        <div>
            <img src="https://sagot.dev/js_logo_rond.svg" alt="Logo Jules Sagot--Gentil"></img>
            <div><b>Jules SAGOT</b></div>
            <div>Développeur Fullstack</div>
            <br/><br/>
            <div>Tel: <a href="tel:+33781898741">+33 (0)7 81 89 87 41</a></div>
            <div>Courriel: <a href="mailto:jules@sagot.dev">jules@sagot.dev</a></div>
            <div>Blog: <a href="https://sagot.dev/articles">sagot.dev</a></div>
            <div>44980 Sainte Luce sur Loire</div>
        </div>
    </body>
</html>
{{< /highlight >}}

J'ai également placé une balise `b` (comme bold qui signifie en gras)
autour de mon nom et deux retours à la ligne avec la balise `br`
(comme break dans line break) qui insère un retour à la ligne. entre
ma fonction et mon numéro de téléphone.

![Cinquième étape de la carte de visite en HTML](/signature_mail/fifth_step.webp)

Voilà le résultat final. Il ne reste plus qu'à l'importer dans un client mail!

## Importation du résultat dans Thunderbird

[Thunderbird](https://www.thunderbird.net/fr/) est un client mail qui permet de stocker localement les
messages à la différence d'un client web.

On va copier le contenu du fichier HTML obtenu lors du tutoriel vers
la signature d'un de vos compte mail dans Thunderbird. Cela permet
d'ajouter automatiquement votre carte de visite à la fin de chaque
mail.

Allez dans `Paramètres du compte` puis collez la carte de visite dans
`Texte de signature` et cochez la case `Utiliser HTML`.

![Insertion de la carte de visite HTML dans Thunderbird](/signature_mail/sixth_step.webp)

La prochaine fois que vous composerez un mail sur Thunderbird, vous
verrez votre signature ajoutée à la fin du message 🤯

![Carte de visite HTML lors d'un nouveau message](/signature_mail/seventh_step.webp)

J'espère que cet article vous a aidé à mieux comprendre HTML par
l'exemple. Si vous avez besoin de réaliser des projets web un poil
plus complexes qu'une carte de visite, contactez moi! Je peux me
déplacer en mission à Nantes et Brest ainsi que dans la région ouest en
général 😄
