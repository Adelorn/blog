---
title: My Ansible articles
website: https://www.ansible.com/
image: "/ansible.jpeg"
tag_description: >
  Ansible is a FOSS used to automate software installation and
  configuration on multiple remote servers. This software is made for
  sysadmins that want to automate their maintenance/deployment
  tasks. Software ingineers can also benefit of the use of Ansible since
  they can see how their app is deployed. They can even contribute to
  the Ansible code (DevOps approach).
description: >
  Ansible allow DevOps to automate IT system management. Discover how to
  use Ansible with concrete examples in my tutorials.
---
