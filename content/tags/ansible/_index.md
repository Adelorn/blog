---
title: Mes articles et tutoriels Ansible
website: https://www.ansible.com/
image: "/ansible.jpeg"
tag_description: >
  Ansible est un logiciel libre qui permet d'automatiser la gestion des
  logiciels installés sur vos serveurs ainsi que leur configuration.
  Ce logiciel est à destination des administrateurs système souhaitant automatiser
  leurs tâches de déploiement/maintenance.
  Les développeurs peuvent aussi bénéficier de l'utilisation d'Ansible, car cela leur
  permet de voir comment leur application est déployée et de proposer des modifications
  aux administrateurs système si nécéssaire (approche DevOps).
description: >
  Ansible permet d'automatiser la gestion des systèmes d'information.
  Mes articles vous permettent d'apprendre Ansible
  avec des exemples concrets.
---
