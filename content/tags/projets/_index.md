---
title: Quelques projets que j'ai réalisé
website: ""
image: "/roues_dentees.png"
tag_description: >
  Réussir un projet, ce n'est pas seulement produire un code de qualité, c'est
  aussi prendre le temps de bien analyser les besoins de l'utilisateur afin fournir un
  logiciel adapté aux attentes des entreprise. Trouver
  la solution technique la plus simple et robuste possible est également indispensable.
  C'est dans cette optique que j'ai réalisé différents projets
  dont je détaille la démarche de réalisation ci-dessous.
description: >
  Voici les projets que j'ai réalisé pour mes clients.
  Je détaille ma façon de réaliser rapidement un
  logiciel robuste et conforme aux attentes.
---
