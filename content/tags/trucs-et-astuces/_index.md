---
title: Guides et astuces d'un informaticien
website: ""
image: "/bulb.png"
tag_description: >
  Je partage régulièrement des tutoriels et astuces pour vous aider
  dans l'apprentissage des bases de l'informatique et l'utilisation
  de logiciels. Les articles sur les langages de programmation vous
  permettent de mieux comprendre le travail d'un informaticien.
  Les guides pratiques sur des logiciels vous aident à utiliser
  plus efficacement votre ordinateur.
description: >
  Apprenez en plus sur l'outil informatique avec mes guides et
  tutoriels pour tous les profils.
---
